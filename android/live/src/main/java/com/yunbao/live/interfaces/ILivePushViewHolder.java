package com.yunbao.live.interfaces;


// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------

public interface ILivePushViewHolder  {

    /**
     * 设置推流监听
     */
    void setLivePushListener(LivePushListener livePushListener);
    /**
     * 开始推流
     */
    void startPush(String pushUrl);
    /**
     * 切换闪光灯
     */
    void toggleFlash();

    /**
     * 切换摄像头
     */
    void toggleCamera();
    /**
     * 是否打开过相机，预览的时候，开启相机会夺取摄像头
     */
    void setOpenCamera(boolean openCamera);


}
