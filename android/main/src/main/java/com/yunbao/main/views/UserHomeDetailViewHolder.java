package com.yunbao.main.views;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.live.bean.SearchUserBean;
import com.yunbao.live.views.AbsUserHomeViewHolder;
import com.yunbao.main.R;
// +----------------------------------------------------------------------
// | Created by Yunbao
// +----------------------------------------------------------------------
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: https://gitee.com/yunbaokeji/
// +----------------------------------------------------------------------
// | Date: 2022-02-17
// +----------------------------------------------------------------------
/**
 * 用户个人中心资料
 */

public class UserHomeDetailViewHolder extends AbsUserHomeViewHolder {

    private String mToUid;
    private TextView mSign;
    private TextView mBirthday;
    private TextView mCity;

    public UserHomeDetailViewHolder(Context context, ViewGroup parentView, String toUid, boolean self) {
        super(context, parentView, toUid, self);
    }

    @Override
    protected void processArguments(Object... args) {
        mToUid = (String) args[0];
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_user_home_detail;
    }

    @Override
    public void init() {
        if (TextUtils.isEmpty(mToUid)) {
            return;
        }
        mSign = (TextView) findViewById(R.id.sign);
        mBirthday = (TextView) findViewById(R.id.birthday);
        mCity = (TextView) findViewById(R.id.city);

    }



    @Override
    public void loadData() {
    }


    public void showData(SearchUserBean userBean, JSONObject obj) {
        if (mSign != null) {
            mSign.setText(userBean.getSignature());
        }
        if (mBirthday != null) {
            mBirthday.setText(StringUtil.contact(WordUtil.getString(R.string.edit_profile_birthday), "： ", obj.getString("birthday")));
        }
        if (mCity != null) {
            mCity.setText(StringUtil.contact(WordUtil.getString(R.string.edit_profile_city), "： ", obj.getString("location")));
        }

    }

    public void refreshData(SearchUserBean userBean, JSONObject obj) {
        if (mSign != null) {
            mSign.setText(userBean.getSignature());
        }
        if (mBirthday != null) {
            mBirthday.setText(StringUtil.contact(WordUtil.getString(R.string.edit_profile_birthday), "： ", obj.getString("birthday")));
        }
        if (mCity != null) {
            mCity.setText(StringUtil.contact(WordUtil.getString(R.string.edit_profile_city), "： ", obj.getString("location")));
        }
    }


}
